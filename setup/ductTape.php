<?php

	$bullWhip = json_decode(file_get_contents("../api/.bullwhip.json"),true);

	// echo __DIR__;
	$filePath = "../api";

	// exit;

	foreach($bullWhip["API_CONFIG"]["URL_MAPPER"] as $endpoint => $controllerName) {
		
		$fileName = "$filePath/controllers/$controllerName.class.php";

		if(!file_exists($fileName)) {
			$controllerCode = file_get_contents('controller.code');
			$patterns = array('/cw:className/');
			$replaceWith = array($controllerName);
			file_put_contents($fileName,preg_replace($patterns, $replaceWith, $controllerCode));
		}
	}

?>