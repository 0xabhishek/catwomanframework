<?php

	class MoviesController extends BaseController{

		private $actions = array(
			
		);

		public function developView($params) {
			
			$verb = $params["verb"];
			unset($params["verb"]);

			switch($verb) {
				case "get": {
					
					// Write your query with Joins until WHERE clause 
					$query = "SELECT * FROM movies WHERE ";

					//these two variables work like 2 buckets, each holding a query condition and its value to be bound
					$qParams = array();
					$queryArr = array();

					// for params set in GET Request, check them here and add them to queryArr and qParams as shown below
					
					// limit by movieId
					if(isset($params["movieId"]) && !empty($params["movieId"])) {
						$queryArr[] = "movie_id=?";
						$qParams[] = $params["movieId"];
					}

					// limit by ratingAbove
					if(isset($params["ratingAbove"]) && !empty($params["ratingAbove"])) {
						$queryArr[] = "rating >= ?";
						$qParams[] = $params["ratingAbove"];
					}

					//NOTE: you can copy paste the above code to add more params to allow API control from GET Request

					// use this to set limit for pagination: 25 is the default max limit here. you can change it
					$limit = 20;
					if(isset($params["limit"]) && !empty($params["limit"])) {
						$limit = intval($params["limit"]);
						$limit = $limit <= 25 ? $limit : 25;
					}

					$limitStart = 0;
					if(isset($params["page"]) && !empty($params["page"]) && $params["page"] > 0) {
						$page = intval($params["page"]) - 1;

						$limitStart = $page * $limit;
					}

					// this creates the query params and joins them to the main query
					$query .= count($queryArr) > 0 ? implode(" AND ",$queryArr) : 1;

					// order and limit your query if needed
					$query .= " ORDER BY movie_id DESC LIMIT $limitStart,$limit";

					// uncomment the line immediately below this comment to verify your query
					// echo $query;print_r($qParams);exit;

					//run your query here

					$db = new PDOWrapper();
					$dataTemp = $db->pdoQuery($query,$qParams)->results();
					$data = array();
					
					foreach($dataTemp as $datum) {
						$queryGenre = "SELECT * FROM genres g LEFT OUTER JOIN movie_genres mg ON g.genre_id=mg.genre_id WHERE mg.movie_id=?";
						$datum["genres"] = $db->pdoQuery($queryGenre,array($datum['movie_id']))->results();

						$queryDirector = "SELECT * FROM directors d LEFT OUTER JOIN movie_directors md ON d.director_id=md.director_id WHERE md.movie_id=?";
						$datum["directors"] = $db->pdoQuery($queryDirector,array($datum['movie_id']))->results();

						$queryWriter = "SELECT * FROM writers w LEFT OUTER JOIN movie_writers mw ON w.writer_id=mw.writer_id WHERE mw.movie_id=?";
						$datum["writers"] = $db->pdoQuery($queryWriter,array($datum['movie_id']))->results();

						$queryActor = "SELECT * FROM actors a LEFT OUTER JOIN movie_actors ma ON a.actor_id=ma.actor_id WHERE ma.movie_id=?";
						$datum["actors"] = $db->pdoQuery($queryActor,array($datum['movie_id']))->results();
						$data[] = $datum;
					}

					// use the response manager class to define your final response. You could only set "data" too. And even data is optional but that would be pointless, would'nt it?
					return ResponseManager::getResponse(array("data"=>$data,"status"=>array("message"=>"Movies data fetched"),"rootParams"=>array("page"=>1),"params"=>$params));
				}
				
				case "post": {
					// Limit POST request with 403
					return ResponseManager::getResponse(array("status"=>array("message"=>"POST method not allowed","code"=>403,"setHTTPStatus"=>true),"params"=>$params));
				}
				
				// you could handle the above cases using the default itself if youre sure the post put and delete is not required. Both ways are ok
				default: {
					return ResponseManager::getResponse(array("status"=>array("message"=>strtoupper($verb)." method not allowed","code"=>403,"setHTTPStatus"=>true),"params"=>$params));
				}
			}
		}
	}

?>