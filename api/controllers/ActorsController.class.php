<?php

	class ActorsController extends BaseController{

		private $actions = array(
			
		);

		public function developView($params) {
			
			$verb = $params["verb"];
			unset($params["verb"]);

			switch($verb) {
				case "get": {
					$data = array();
					return ResponseManager::getResponse(array("data"=>$data,"status"=>array("message"=>"ActorsController data fetched"),"rootParams"=>array("page"=>1),"params"=>$params));
				}
				
				case "post": {
					// Limit POST request with 403
					return ResponseManager::getResponse(array("status"=>array("message"=>"POST method not allowed","code"=>403,"setHTTPStatus"=>true),"params"=>$params));
				}
				
				// you could handle the above cases using the default itself if youre sure the post put and delete is not required. Both ways are ok
				default: {
					return ResponseManager::getResponse(array("status"=>array("message"=>strtoupper($verb)." method not allowed","code"=>403,"setHTTPStatus"=>true),"params"=>$params));
				}
			}
		}
	}

?>